using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private float speed;

    private float inputX, inputY;
    private Vector2 movement;

    private Rigidbody2D rb;
    private Animator anim;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    public void SetInput(float x , float y)
    {
        inputX = x;
        inputY = y;
    }

    private void Update()
    {
        SetInput(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        movement = new Vector2(inputX, inputY).normalized;

        //flip
        if (inputX != 0)
        {
            transform.localScale = new Vector3(inputX, transform.localScale.y, transform.localScale.z);
            anim.Play("Walk");
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = movement * speed;
    }
}
