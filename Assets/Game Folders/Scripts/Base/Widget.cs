using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Widget : MonoBehaviour
{
    public WidgetName namaWidget;

    protected virtual void Start()
    {
        Button[] allButtons = GetComponentsInChildren<Button>(true);
        if (allButtons.Length > 0)
        {
            foreach (var item in allButtons)
            {
                //item.onClick.AddListener(() => AudioManager.Instance.PlaySound("Klik"));
            }
        }
    }

    protected virtual void CloseWidget()
    {
        gameObject.SetActive(false);
    }
}
