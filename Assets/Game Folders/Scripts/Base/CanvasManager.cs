using GaweDeweStudio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] private Page[] allPages;
    [SerializeField] private Widget[] allWidgets;

    protected virtual void OnEnable()
    {
        allPages = GetComponentsInChildren<Page>(true);
        allWidgets = GetComponentsInChildren<Widget>(true);
    }

    protected void SetPage(PageName findPage)
    {
        foreach (var p in allPages)
        {
            p.gameObject.SetActive(false);
        }

        Page tempPage = Array.Find(allPages, p => p.namaPage == findPage);
        if(tempPage != null)
        {
            tempPage.gameObject.SetActive(true);
        }
    }

    protected void ShowWidget(WidgetName findWidget)
    {
        Widget tempWidget = Array.Find(allWidgets, w => w.namaWidget == findWidget);
        if(tempWidget != null)
        {
            tempWidget.gameObject.SetActive(true);
        }
    }
}
