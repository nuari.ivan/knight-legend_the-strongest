using GaweDeweStudio;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Page : MonoBehaviour
{
    public PageName namaPage;

    protected virtual void Start()
    {
        Button[] allButtons = GetComponentsInChildren<Button>(true);
        if(allButtons.Length>0)
        {
            foreach (var item in allButtons)
            {
                //item.onClick.AddListener(() => AudioManager.Instance.PlaySound("Klik"));
            }
        }
    }

    protected virtual void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
