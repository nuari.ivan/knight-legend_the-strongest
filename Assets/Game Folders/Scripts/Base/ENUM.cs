namespace GaweDeweStudio
{
    public enum GameState
    {
        Menu,
        Setting,
        Market,
        Game,
        Pause,
        Inventory,
        Loading,
        Credits
    }

    public enum PageName
    {
        Menu,
        Setting,
        Market,
        Inventory,
        Loading,
        Credits
    }

    public enum WidgetName
    {
        Exit,
        Pause
    }
}