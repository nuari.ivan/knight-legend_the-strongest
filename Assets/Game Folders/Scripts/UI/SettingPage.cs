using GaweDeweStudio;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingPage : Page
{
    [SerializeField] private Button buttonHome;
    [SerializeField] private Slider sliderVolumeMusic;
    [SerializeField] private TMP_Dropdown dropdownLanguage;

    protected override void Start()
    {
        base.Start();

        buttonHome.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        sliderVolumeMusic.onValueChanged.AddListener((float val) => Debug.Log($"Set Volume to{val * 10f}"));
        dropdownLanguage.onValueChanged.AddListener((OnDropdownSelect));
    }

    private void OnDropdownSelect(int arg0)
    {
        Debug.Log($"Select Language to : {dropdownLanguage.options[arg0].text}");
    }
}
