using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasManager : CanvasManager
{
    private void Start()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }
    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                SetPage(PageName.Menu);
                break;
            case GameState.Setting:
                SetPage(PageName.Setting);
                break;
            case GameState.Market:
                SetPage(PageName.Market);
                break;
            case GameState.Game:
                
                break;
            case GameState.Pause:
                break;
            case GameState.Inventory:
                SetPage(PageName.Inventory);
                break;
            case GameState.Loading:
                break;
        }
    }
}
