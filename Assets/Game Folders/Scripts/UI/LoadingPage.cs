using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPage : Page
{
    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            if(Input.GetMouseButtonDown(0))
            {
                GameManager.Instance.ChangeState(GameState.Menu);
            }
        }
    }
}
