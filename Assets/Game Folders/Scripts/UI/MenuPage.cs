using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button buttonNew;
    [SerializeField] private Button buttonLoad;
    [SerializeField] private Button buttonSetting;
    [SerializeField] private Button buttonQuit;
    [SerializeField] private Button buttonCredits;

    protected override void Start()
    {
        base.Start();

        buttonNew.onClick.AddListener(() => ChangeScene("Gameplay"));
        buttonLoad.onClick.AddListener(() => { });
        buttonSetting.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Setting));
        buttonQuit.onClick.AddListener(() => Application.Quit());
        buttonCredits.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Credits));
    }
}
