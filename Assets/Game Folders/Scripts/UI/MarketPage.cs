using GaweDeweStudio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketPage : Page
{
    [SerializeField] private Button buttonHome;

    protected override void Start()
    {
        base.Start();

        buttonHome.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
    }
}
